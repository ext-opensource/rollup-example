import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { babel } from "@rollup/plugin-babel";
import json from "@rollup/plugin-json";
import eslint from "@rollup/plugin-eslint";
import terser from '@rollup/plugin-terser';
import replace from '@rollup/plugin-replace';
import serve from 'rollup-plugin-serve';
import livereload from "rollup-plugin-livereload";
//import pkg from "./package.json" assert { type: "json" };
import alias from '@rollup/plugin-alias';
import path from 'path';

const production = process.env.NODE_ENV;
const ver = process.env.npm_package_version;
const dev = (process.env.NODE_ENV === "development");
const customServer = dev;
const projectRootDir = path.resolve();

const customResolver = resolve({
  extensions: ['.mjs', '.js', '.jsx', '.json', '.sass', '.scss']
});

console.log(projectRootDir);
console.log(production);
console.log(ver);

export default {
  input: "src/index.js",
  external: ['lodash'],    // 指出哪些模块需要被视为外部引入
  output: [
    {
      file: `./public/js/index.js`,
      format: "umd",  //cjs,esm,iife,umd
      name: 'Hello',
      exports: "auto",  //"auto" | "default"| "named"| "none"
      sourcemap: true,
      banner: "/** banner */", //为打包好的文件添加注释，注释的位置在整个文件的首行
      footer: "/** footer */", //为打包好的文件添加注释，注释的位置在整个文件的尾行
      intro: "/** intro */", //为打包好的文件添加注释，注释的位置在打包数据内容的头部
      outro: "/** outro */", //为打包好的文件添加注释，注释的位置在打包数据内容的末尾
    },
  ],
  plugins: [
    resolve(),
    commonjs(),
    json(),
    //terser(),
    babel({
      babelHelpers: "bundled",
      presets: ["@babel/preset-env"],
    }),
    eslint({
      include: ['src/**/*.js'] // 需要检查的部分
    }),

    replace({
      preventAssignment: true,
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),

    // alias({
    //   entries: [
    //     {
    //       find: 'xxxx',
    //       replacement: path.resolve(projectRootDir, 'src')
    //     }
    //   ],
    //   customResolver
    // }),

    customServer && serve({
      open: true,
      contentBase: ['public'],
      //host: 'localhost',
      port: 10001,
    }),
    customServer && livereload(),
  ],
}
