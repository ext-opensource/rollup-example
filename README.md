# rollup样例代码
   https://gitee.com/ext-opensource/rollup-example

#### 常用组件
    npm install --save-dev rollup
    npm install --save-dev @rollup/plugin-node-resolve //允许加载第三方模块
    npm install --save-dev @rollup/plugin-commonjs //支持CommonJS模块代码
    npm install --save-dev @rollup/plugin-json  //可以将.json文件转为es6模块供rollup处理
    npm install --save-dev @rollup/plugin-babel //用于转换es6语法
    npm install --save-dev @babel/preset-env //js语法转换
    npm install --save-dev @rollup/plugin-eslint //js规范提示
    npm install --save-dev @rollup/plugin-terser //进行打包代码压缩。
    npm install --save-dev @rollup/plugin-replace //进行变量替换
    npm install --save-dev @rollup/plugin-alias //路径别名
    npm install --save-dev cross-env  //它可根据不同的系统设置环境变量
    npm install --save-dev rollup-plugin-serve //html服务器
    npm install --save-dev rollup-plugin-livereload //html服务器热加载

    npm install --save-dev jest jest-environment-jsdom  //js测试

#### 与gulp结合组件
    npm install --save-dev gulp
    npm install --save-dev gulp-clean  //清空文件
    npm install --save-dev gulp-uglify //js压缩
    npm install --save-dev gulp-clean-css //css压缩
    npm install --save-dev gulp-htmlmin //html压缩
    npm install --save-dev gulp-html-version //js、css文件加版本号
    npm install --save-dev gulp-remove-html //处理html
    npm install --save-dev gulp-remove-empty-lines //删除空行
    npm install --save-dev browser-sync //html服务器