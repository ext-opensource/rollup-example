/** banner */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.Hello = factory());
})(this, (function () { 'use strict';

  /** intro */

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function _toPrimitive(input, hint) {
    if (typeof input !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (typeof res !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }
  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return typeof key === "symbol" ? key : String(key);
  }

  var __eval = eval;
  var THellWorld = /*#__PURE__*/function () {
    function THellWorld(prop) {
      _classCallCheck(this, THellWorld);
    }
    _createClass(THellWorld, [{
      key: "print",
      value: function print() {
        console.log(this.getString());
      }
    }, {
      key: "getString",
      value: function getString() {
        return "Hello Wrold !!!";
      }
    }, {
      key: "run",
      value: function run(text) {
        console.log(text);
        __eval(text);
      }
    }]);
    return THellWorld;
  }();
  var HellWorld = new THellWorld();
  console.log("watch");

  return HellWorld;

  /** outro */

}));
/** footer */
//# sourceMappingURL=index.js.map
