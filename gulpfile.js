const gulp = require('gulp');
const clean = require('gulp-clean');  //删除文件的插件
const uglify = require('gulp-uglify'); //压缩JS文件的插件
const cssmin = require('gulp-clean-css'); //压缩CSS文件的插件
const autoprefixer = require('gulp-autoprefixer');
const htmlmin = require('gulp-htmlmin'); //压缩HTML文件的插件
const gulpHtmlVersion = require('gulp-html-version'); //js,css引用后加版本号
const removeHtml = require('gulp-remove-html'); //标签清除
const removeEmptyLines = require('gulp-remove-empty-lines'); //清除空白行
const browserSync = require('browser-sync'); //新建一个本地服务

//设置源文件和输出文件的目录配置
const _dir = {
    "srcDir": "./public",
    "destDir": "./dist",
};

const path_src = {
    "path": _dir.srcDir + "/",
    "html": _dir.srcDir + "/",
    "css": _dir.srcDir + "/css/",
    "js": _dir.srcDir + "/js/",
    "image": _dir.srcDir + "/image/",
    "lib": _dir.srcDir + "/lib/",
    "component": _dir.srcDir + "/component/",
};
const path_dest = {
    "path": _dir.destDir + "/",
    "html": _dir.destDir + "/",
    "css": _dir.destDir + "/css/",
    "js": _dir.destDir + "/js/",
    "image": _dir.destDir + "/image/",
    "lib": _dir.destDir + "/lib/",
    "component": _dir.destDir + "/component/"
};

const copy_files = [
    _dir.srcDir + '/compent/**',
    _dir.srcDir + '/image/**',
    _dir.srcDir + '/favicon.png',
];

gulp.task('html', function () {
    const options = {
        removeComments: true, //清除HTML注释
        collapseWhitespace: true, //压缩HTML
        collapseBooleanAttributes: false, //省略布尔属性的值 <input checked="true"/> ==> <input />
        removeEmptyAttributes: false, //删除所有空格作属性值 <input id="" /> ==> <input />
        removeScriptTypeAttributes: false, //删除<script>的type="text/javascript"
        removeStyleLinkTypeAttributes: false, //删除<style>和<link>的type="text/css"
        minifyJS: true, //压缩页面JS
        minifyCSS: true //压缩页面CSS
    };

    return gulp.src(path_src.html + '*.html')
        .pipe(gulpHtmlVersion())  //js css 添加版本号
        .pipe(removeHtml()) //清除特定标签
        .pipe(removeEmptyLines({
            removeComments: true
        })) //清除空白行
        .pipe(htmlmin((options)))
        .pipe(gulp.dest(path_dest.html));
});

gulp.task('js', function () {
    return gulp.src(path_src.js + '*.js')
        .pipe(uglify())
        .pipe(gulp.dest(path_dest.js));
});

gulp.task('css', function () {
    return gulp.src(path_src.css + '*.css')
        .pipe(cssmin())
        .pipe(autoprefixer())
        .pipe(gulp.dest(path_dest.css));
});

gulp.task('clean', function () {
    return gulp.src(_dir.destDir, {read: false,allowEmpty:true})
        .pipe(clean());
});

gulp.task('copy', function () {
    return gulp.src(copy_files, { base: _dir.srcDir }).pipe(gulp.dest(path_dest.path));
});

gulp.task("build",
    gulp.series('clean',
        gulp.parallel("js", "css", "html", "copy")
    )
);

gulp.task('browserSync', function () {
    browserSync.init({
        port: 8024,
        watch: true,
        server: {
            //directory: true, //显示目录结构
            baseDir: _dir.destDir,      // 启动服务的目录 默认 index.html
            index: 'index.html',        // 自定义启动文件名
            reloadDelay: 2000,
        }
    });
});